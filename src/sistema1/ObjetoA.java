package sistema1;

public class ObjetoA extends Observable {

	private int _dato = 0;
	

	public int get_dato() {
		return _dato;
	}

	public void set_dato(int _dato) {
		this._dato = _dato;
		this.notificar();
	}

}
