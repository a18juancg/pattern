package sistema2;

import sistema1.ObjetoA;
import sistema1.Observer;

public class ObjetoB implements Observer {

	private int _datoLeido;
	private ObjetoA _objDelQueDependo;
	
	public int get_datoLeido() {
		return _datoLeido;
	}

	public void set_datoLeido(int _datoLeido) {
		this._datoLeido = _datoLeido;
	}

	public ObjetoA get_objDelQueDependo() {
		return _objDelQueDependo;
	}

	public void set_objDelQueDependo(ObjetoA _objDelQueDependo) {
		this._objDelQueDependo = _objDelQueDependo;
		this._objDelQueDependo.añadirObserver(this);
	}
	
	
	//Constructor
	public ObjetoB(ObjetoA pObjDelQueDependo){
		this.set_objDelQueDependo(pObjDelQueDependo);
	}
	
	@Override
	public void actualizar() {
		this.set_datoLeido(this.get_objDelQueDependo().get_dato());

	}



}
